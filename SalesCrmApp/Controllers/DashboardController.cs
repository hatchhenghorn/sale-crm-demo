using Microsoft.AspNetCore.Mvc;

namespace SalesCrmApp.Controllers;

public class DashboardController : Controller
{
    [Route("dashboard")]
    public IActionResult Index()
    {
        return View();
    }
}