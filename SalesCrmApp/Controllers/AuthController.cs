using Microsoft.AspNetCore.Mvc;

namespace SalesCrmApp.Controllers;

public class AuthController : Controller
{
    [Route("login")]
    [Route("/")]
    public IActionResult Login()
    {
        return View();
    }
}